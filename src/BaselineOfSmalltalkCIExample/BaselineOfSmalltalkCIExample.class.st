"
I define the Baseline of the project SmalltalkCIExample
"
Class {
	#name : #BaselineOfSmalltalkCIExample,
	#superclass : #BaselineOf,
	#category : #BaselineOfSmalltalkCIExample
}

{ #category : #baselines }
BaselineOfSmalltalkCIExample >> baseline: spec [
	<baseline>
	spec for: #common do: [
		"Packages"
		spec
			package: 'SmalltalkCIExample';
			package: 'SmalltalkCIExample-Tests' with: [ spec requires: #('SmalltalkCIExample') ]
	].
]

{ #category : #baselines }
BaselineOfSmalltalkCIExample >> projectClass [
	^ MetacelloCypressBaselineProject
]
