"
I represent a Commit with an author, message, and timestamp
"
Class {
	#name : #Commit,
	#superclass : #Object,
	#instVars : [
		'author',
		'message',
		'timestamp',
		'lGitCommit',
		'gitHash',
		'repository'
	],
	#category : #SmalltalkCIExample
}

{ #category : #'instance creation' }
Commit class >> fromLGitCommit: anLGitCommit onRepository: aRepository [ 
	^ self new
		lGitCommit: anLGitCommit;
		gitHash: anLGitCommit id hexString;
		author: anLGitCommit author name;
		message: anLGitCommit message;
		timestamp: anLGitCommit time asDateAndTime;
		repository: aRepository;
		yourself
]

{ #category : #accessing }
Commit >> author [
	^ author
]

{ #category : #accessing }
Commit >> author: anObject [
	author := anObject
]

{ #category : #accessing }
Commit >> gitHash: aString [ 
	gitHash := aString
]

{ #category : #accessing }
Commit >> lGitCommit [
	(lGitCommit isNil or: [ lGitCommit isReady not ]) ifTrue: [
		lGitCommit := self loadLGitCommit
	].
	^ lGitCommit
]

{ #category : #accessing }
Commit >> lGitCommit: aLGitCommit [ 
	lGitCommit := aLGitCommit
]

{ #category : #'accessing-computed' }
Commit >> loadLGitCommit [
	lGitCommit ifNil: [
		^ LGitCommit of: self repository lGitRepository fromHexString: self gitHash
	] ifNotNil: [ :notNilLGitCommit |
		notNilLGitCommit repository isInitialized ifFalse: [
			notNilLGitCommit repository open.
			notNilLGitCommit repository checkout: 'master'
		].
		notNilLGitCommit isReady ifFalse: [ notNilLGitCommit initializeWithId: notNilLGitCommit id ].
		^ notNilLGitCommit
	]
]

{ #category : #accessing }
Commit >> message [
	^ message
]

{ #category : #accessing }
Commit >> message: anObject [
	message := anObject
]

{ #category : #accessing }
Commit >> repository [
	^ repository
]

{ #category : #accessing }
Commit >> repository: aRepository [ 
	repository := aRepository
]

{ #category : #accessing }
Commit >> timestamp [
	^ timestamp
]

{ #category : #accessing }
Commit >> timestamp: anObject [
	timestamp := anObject
]
