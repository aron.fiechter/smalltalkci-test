"
A RepositoryTest is a test class for testing the behavior of Repository
"
Class {
	#name : #RepositoryTest,
	#superclass : #TestCase,
	#instVars : [
		'repo'
	],
	#category : #'SmalltalkCIExample-Tests'
}

{ #category : #running }
RepositoryTest >> setUp [
	"Hooks that subclasses may override to define the fixture of test."
	repo := Repository fromGitHubUrl: 'https://github.com/TiredFalcon/progress-bar.git'
]

{ #category : #tests }
RepositoryTest >> testCommits [
	self assert: repo commits size equals: 2
]

{ #category : #tests }
RepositoryTest >> testCreation [
	self assert: repo url equals: 'https://github.com/TiredFalcon/progress-bar.git'.
	self assert: repo name equals: 'TiredFalcon-progress-bar'
]
