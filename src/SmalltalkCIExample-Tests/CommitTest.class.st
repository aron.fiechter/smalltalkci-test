"
A CommitTest is a test class for testing the behavior of Commit
"
Class {
	#name : #CommitTest,
	#superclass : #TestCase,
	#instVars : [
		'repo'
	],
	#category : #'SmalltalkCIExample-Tests'
}

{ #category : #running }
CommitTest >> setUp [
	"Hooks that subclasses may override to define the fixture of test."
	repo := Repository fromGitHubUrl: 'https://github.com/TiredFalcon/progress-bar.git'
]

{ #category : #tests }
CommitTest >> testCreationFromRepo [
	| commit |
	commit := repo commits first.
	self assert: commit author isNotNil.
	self assert: commit message equals: 'Example code using progress-bar to show progress in execution. The progress bar is really still nested inside the program''s code. TODO: think of a way to separate it.', String lf.
	self assert: commit timestamp equals: (DateAndTime fromString: '2017-07-07T17:02:49+02:00').

]

{ #category : #tests }
CommitTest >> testEmpty [
	| commit |
	commit := Commit new.
	
	self assert: commit author isNil.
	self assert: commit message isNil.
	self assert: commit timestamp isNil

]

{ #category : #tests }
CommitTest >> testLGitCommit [
	| commit |
	commit := repo commits first.
	self assert: commit lGitCommit isNotNil

]

{ #category : #tests }
CommitTest >> testRepository [
	| commit |
	commit := repo commits first.
	self assert: commit repository equals: repo
]
